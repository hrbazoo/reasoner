.PHONY: all

all: Reasoner.class
	@@echo "Done"

Reasoner.class: Reasoner.java data/Triple.java
	javac Reasoner.java

clean:
	@find . -name "*.class" -exec rm -f {} \;


