import java.lang.Exception;
import java.lang.StringBuilder;

import java.io.BufferedReader;
import java.io.FileReader;

import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.Scanner;
import java.util.Iterator;

import data.Triple;
import rdf.Rdf;
import rdfs.Rdfs;
import owl.Owl;

public class Reasoner extends Object {

  private static long threshold             = 1000000;
  private static long tripleCounter         = 0;
	private static long subclassTripleCounter = 0;
	private static long subclassTripleDups    = 0;
	private static long typeTripleDups        = 0;
	private static long typeTripleCounter     = 0;
	private static long invalidTriples        = 0;
  /* ----------------------------------- */
  private static Map<String, Set<String>> superclassMap = new HashMap<String, Set<String>>();
  private static Set<Triple> equclassSet = new HashSet<Triple>();
	private static Map<String, Set<String>> classMembersMap = new HashMap<String, Set<String>>();
  /* ----------------------------------- */
  public static void main (String[] argv) {
    long tripleCounter = 0;
    String inputString = null;
    Triple inputTriple = null;
    String subclassInputFile;
		String typeInputFile;

    if (argv.length < 2) {
      System.err.println ("Usage: reasoner <subclass triple file> <type triples file>!");
      System.exit(1);
    }

    subclassInputFile = argv[0];
		typeInputFile = argv[1];
		loadSubclassTriples(subclassInputFile);
    subclassTransitivityReasoning();
		countTypeDerivations (typeInputFile);
		//printSuperclassMap();
		System.exit(1);
//		loadTypeTriples(inputFile);
//		printClassMembersMap();
//		System.exit(0);

//    try {
//      BufferedReader br = new BufferedReader(new FileReader(inputFile));
//      try {
//        while ((inputString = br.readLine()) != null) {
//          try {
//									inputTriple = new Triple(inputString);
//									filterTriple(inputTriple);
//									updateTripleCounter();
//          } catch (Exception e) {
//									invalidTriples++;
//          }
//        }
//      } finally {
//        br.close();
//      }
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//		System.out.println ("All subclass Triples = " + subclassTripleCounter);
//		System.out.println ("Duplicated Triples   = " + subclassTripleDups);
//		System.out.println ("Type Triples   = " + typeTripleCounter);
//		System.out.println ("Type Triple Suplicates  = " + typeTripleDups);
//		System.out.println ("Invalid Triples = " + invalidTriples);
//    //equclassReasoning();
//    subclassTransitivityReasoning();
//		//subclassInheritanceReasoning();
//		//countAllTriples (superclassMap);
//    printSets();
  }
	private static void countTypeDerivations (String inputFile) {
					long totalProcessedTriples = 0;
					long derivedTriples = 0;
					try {
									BufferedReader br = new BufferedReader(new FileReader(inputFile));
									String delim = "\\s";
									String subject;
									String object;
									String inputString;
									try {
													while ((inputString = br.readLine()) != null) {
																	try {
																					String[] terms = inputString.split(delim);
																					if (terms.length < 2) {
																									System.err.println ("Invalid input");
																									continue;
																					}
																					subject = terms[0];
																					object  = terms[1];
																					totalProcessedTriples++;
																					if (superclassMap.get(object) != null) {
																									derivedTriples += superclassMap.get(object).size();
																					}
																					if (totalProcessedTriples % 100000 == 0) {
																									System.out.println ("so far processed " + totalProcessedTriples + " & derived " + derivedTriples + " triples");
																					}
																	} catch (Exception e) {
																	}
													}
									} finally {
													br.close();
									}
					}catch(Exception e) {
									e.printStackTrace();
					}
					System.out.println ("Derived " + derivedTriples + " Type Triples");
	}
  /* ----------------------------------- */
	private static void loadTypeTriples (String inputFile) {
					long counter = 0;
					try {
									BufferedReader br = new BufferedReader(new FileReader(inputFile));
									String delim = "\\s";
									String subject;
									String object;
									String inputString;
									try {
													while ((inputString = br.readLine()) != null) {
																	try {
																					String[] terms = inputString.split(delim);
																					if (terms.length < 2) {
																									System.err.println ("Invalid input");
																									continue;
																					}
																					subject = terms[0];
																					object  = terms[1];
																					if (classMembersMap.get(object) == null) {
																									Set<String> memberSet;
																									memberSet = new HashSet<String>();
																									memberSet.add(subject);
																									classMembersMap.put(object, memberSet);
																					} else {
																									if (classMembersMap.get(object).contains(subject)) {
																													typeTripleDups++;
																									}
																									classMembersMap.get(object).add(subject);
																					}
																					counter++;
																					if (counter % 1000000 == 0) {
																									System.out.println ("imported " + counter + " TYPE triples");
																					}
																	} catch (Exception e) {
																	}
													}
									} finally {
													br.close();
									}
					}catch(Exception e) {
									e.printStackTrace();
					}
					System.out.println ("Total duplicated triples = " + typeTripleDups);
					System.out.println ("Totally parsed " + counter + " Triples");
	}
  /* ----------------------------------- */
	private static void loadSubclassTriples (String inputFile) {
					long counter = 0;
					try {
									BufferedReader br = new BufferedReader(new FileReader(inputFile));
									String delim = "\\s";
									String subject;
									String object;
									String inputString;
									try {
													while ((inputString = br.readLine()) != null) {
																	try {
																					String[] terms = inputString.split(delim);
																					if (terms.length < 2) {
																									System.err.println ("Invalid input");
																									continue;
																					}
																					subject = terms[0];
																					object  = terms[1];
																					if (superclassMap.get(subject) == null) {
																									Set<String> superclassSet;
																									superclassSet = new HashSet<String>();
																									superclassSet.add(object);
																									superclassMap.put(subject, superclassSet);
																					} else {
																									if (superclassMap.get(subject).contains(object)) {
																									}
																									superclassMap.get(subject).add(object);
																					}
																					counter++;
																					if (counter % 1000000 == 0) {
																									System.out.println ("imported " + counter + " subclass triples");
																					}
																	} catch (Exception e) {
																	}
													}
									} finally {
													br.close();
									}
					}catch(Exception e) {
									e.printStackTrace();
					}
					System.out.println ("Totally parsed " + counter + " Triples");
	}
  /* ----------------------------------- */
  private static void filterTriple (Triple triple) {
				long otherTriples = 0;
				if (! (filterTypeTriples (triple) || filterSubClassOf (triple) || filterEquClass (triple))) {
								otherTriples++;
				}
  }
  /* ----------------------------------- */
	private static boolean filterTypeTriples (Triple triple) {
					if (triple.getPredicate().equals(Rdf.TYPE)) {
									typeTripleCounter++;
									if (classMembersMap.get(triple.getObject()) == null) {
													Set<String> memberSet;
													memberSet = new HashSet<String>();
													memberSet.add(triple.getSubject());
													classMembersMap.put(triple.getObject(), memberSet);
									} else {
													if (classMembersMap.get(triple.getObject()).contains(triple.getSubject())) {
																	typeTripleDups++;
													}
													classMembersMap.get(triple.getObject()).add(triple.getSubject());
									}
									return true;
					}
					return false;
	}
  /* ----------------------------------- */
  private static boolean filterSubClassOf (Triple triple) {
    if (triple.getPredicate().equals(Rdfs.SUBCLASSOF)){
			subclassTripleCounter++;
      if (superclassMap.get(triple.getSubject()) == null) {
        Set<String> objectSet;
        objectSet = new HashSet<String>();
        objectSet.add(triple.getObject());
				objectSet.add(triple.getSubject()); // Everything is subclass of itself
        superclassMap.put(triple.getSubject(), objectSet);
      } else {
				if (superclassMap.get(triple.getSubject()).contains(triple.getObject())) {
								subclassTripleDups++;
				}
        superclassMap.get(triple.getSubject()).add(triple.getObject());
      }
			return true;
    }
		return false;
  }
  /* ----------------------------------- */
  private static boolean filterEquClass (Triple triple) {
					if (triple.getPredicate().equals(Owl.EQUCLASS)) {
									equclassSet.add(triple);
									return true;
					}
					return false;
  }
  /* ----------------------------------- */
	private static void subclassInheritanceReasoning() {
					long counter = 0;
					Map<String, Set<String>> temp = new HashMap<String, Set<String>>();
					for (String className: superclassMap.keySet()) {
									//System.out.println ("looking for " + className);
									if (classMembersMap.get(className) != null) {
													for (String superclassName : superclassMap.get(className)) {
																	for (String member : classMembersMap.get(className)) {
																					if (temp.get(superclassName) == null) {
																									Set<String> memberSet  = new HashSet<String>();
																									memberSet.add(member);
																									temp.put(superclassName, memberSet);
																					} else {
																									temp.get(superclassName).add(member);
																					}
																					//System.out.println (member + " " + Rdf.TYPE + " " + superclassName);
																	}
													}
									}
					}
					for (String className : temp.keySet()) {
									if (classMembersMap.get(className) == null) {
													classMembersMap.put(className, temp.get(className));
													counter += temp.get(className).size();
//													System.out.println ("*** " + className + " " + classMembersMap.get(className) + " " + counter);
									} else {
													for (String member : temp.get(className)) {
																	if (!classMembersMap.get(className).contains (member)) {
																					classMembersMap.get(className).add(member);
																					counter++;
//																					System.out.println ("** " + className + " " + member + " " + counter);
																	}
													}
									}
					}
					System.out.println ("Derived subclass inheritance triples = " + counter);
	}
  /* ----------------------------------- */
  private static void subclassTransitivityReasoning() {
					long counter = 0;
					boolean foundNewDerivation;
					Map<String, Set<String>> temp = new HashMap<String, Set<String>>();
					do {
									foundNewDerivation = false;
									temp.clear();
									//System.out.println ("sub0 size = " + superclassMap.keySet().size());
									for (String subject1 : superclassMap.keySet()) {
													Set<String> objects = superclassMap.get(subject1);
													//System.out.println ("set1 size = " + objects.size());
													//System.out.println ("subject1 = " + subject1);
													for (String object : objects) {
														//			System.out.println ("--> " + object);
																	if (superclassMap.get(object) != null) {
																					Set<String> subject2 = superclassMap.get(object);
															//						System.out.println ("set2 size = " + subject2.size());
																					for (String s : subject2) {
																									if (superclassMap.get(subject1).contains(s) == false) {
																													if (temp.get(subject1) == null) {
																																	Set<String> objectSet;
																																	objectSet = new HashSet<String>();
																																	objectSet.add(s);
																																	temp.put(subject1, objectSet);
																													} else {
																																	temp.get(subject1).add(s);
																													}
																													//System.out.println (subject1 + " " + Rdfs.SUBCLASSOF + " " + s + " .");
																									}
																					}
																	}
													}
									}
									for (String s : temp.keySet()) {
													Set<String> t = temp.get(s);
													for (String str : t) {
																	counter++;
																	superclassMap.get(s).add(str);
																	//System.out.println (s + Rdfs.SUBCLASSOF + str);
													}
									}
									if (temp.keySet().size() != 0) {
													foundNewDerivation = true;
									}
									System.out.println ("derived subclass Transitivity so far = " + counter);
					} while (foundNewDerivation);
					System.out.println ("Total subclass Transitivity derivations = " + counter);
  }
  /* ----------------------------------- */
	private static void countAllTriples(Map<String,Set<String>> hashTable) {
					long counter = 0;
					for (String key : hashTable.keySet()) {
									Set<String> values = hashTable.get(key);
									for (String value : values) {
													counter++;
									}
					}
					System.out.println ("Triples in the HashTable = " + counter);
	}
  /* ----------------------------------- */
	private static void equclassReasoning() {
					for (Triple triple: equclassSet) {
									if (superclassMap.get(triple.getSubject()) == null) {
													Set<String> objectSet;
													objectSet = new HashSet<String>();
													objectSet.add(triple.getObject());
													superclassMap.put(triple.getSubject(), objectSet);
									} else {
													superclassMap.get(triple.getSubject()).add(triple.getObject());
									}
									if (superclassMap.get(triple.getObject()) == null) {
													Set<String> subjectSet;
													subjectSet = new HashSet<String>();
													subjectSet.add(triple.getSubject());
													superclassMap.put(triple.getObject(), subjectSet);
									} else {
													superclassMap.get(triple.getObject()).add(triple.getSubject());
									}
					}
	}
  /* ----------------------------------- */
  private static void printSets() {
		long total = 0;
		long max = 0;
		List<Long> list = new ArrayList<Long>();
		long counter = 0;
		System.out.println ("#SuperClasses = " + superclassMap.keySet().size());
		for (String key : superclassMap.keySet()) {
						counter = 0;
						//System.out.print ("Key = " + key + " has ");
						for (String value : superclassMap.get(key)) {
										counter++;
										total++;
										//System.out.println (key + " " + "<http://www.w3.org/2000/01/rdf-schema#subClassOf>" + " " + value + " .");
						}
						//if (counter > 1000) { list.add (counter); }
						//System.out.println (counter + " members");
		}
//		for (long val : list) {
//						System.out.println ("** " + val);
//		}
		System.out.println ("total = " + total + " Avg = " + (total / superclassMap.keySet().size()));
  }
  /* ----------------------------------- */
  private static void printClassMembersMap() {
		for (String key : classMembersMap.keySet()) {
//						System.out.println ("Key = " + key);
						for (String value : classMembersMap.get(key)) {
										System.out.println (key + " " + Rdf.TYPE + " " + value + " .");
						}
		}
  }
  /* ----------------------------------- */
  private static void printSuperclassMap() {
		for (String key : superclassMap.keySet()) {
//						System.out.println ("Key = " + key);
						for (String value : superclassMap.get(key)) {
										System.out.println (key + " " + Rdfs.SUBCLASSOF + " " + value + " .");
						}
		}
  }
  /* ----------------------------------- */
  private static void updateTripleCounter() {
    if (tripleCounter % threshold == 0) {
      System.out.println (tripleCounter);
    }
    tripleCounter++;
  }
}
