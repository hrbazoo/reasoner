package data;

import java.lang.Exception;
import org.apache.commons.lang.builder.HashCodeBuilder;
/* ----------------------------- */
public class Triple extends Object {

  private static final String delim = "\\s";
  /* ----------------------------- */
  private String subject   = null;
  private String predicate = null;;
  private String object    = null;;
  /* ----------------------------- */
  boolean validity = true;
  /* ----------------------------- */
  private boolean isValidTriple (String tripleString) {
    return tripleString.matches (
        "^"                                +
        "\\s?(\".*\"\\^\\^)?<.*>"          + // Subject URI
        "\\s+(\".*\"\\^\\^)?<.*>"          + // Predicate URI
        "\\s+((\".*\"\\^\\^)?<.*>|\".*\")" + // Object URIs or Literal
        "\\s+\\."                          + // Dot
        "$");
  }
  /* ----------------------------- */
  public Triple (String input) throws Exception {
  /*
   * For the performance reasons, I'm ganna skip this step,
   * Anyways, Wouter is going to check the validity of triples.
   */
  //  if (!isValidTriple(input)) {
  //    validity = false;
  //    return;
  //  }
    /* ----------------------------- */
    String[] terms = input.split(delim);
    /* ----------------------------- */
    if (terms.length > 3) {
      subject   = terms[0];
      predicate = terms[1];
      object    = terms[2];
    } else {
      throw new Exception("invalid Triple");
    }
  }
  /* ----------------------------- */
  public Triple () {
  }
  /* ----------------------------- */
  public Triple (Triple triple) {
		this.subject = triple.subject;
		this.predicate = triple.predicate;
		this.object = triple.object;
  }
  /* ----------------------------- */
  public boolean isValid() {
    return validity;
  }
  /* ----------------------------- */
  public String getSubject() {
    return subject;
  }
  /* ----------------------------- */
  public String getPredicate() {
    return predicate;
  }
  /* ----------------------------- */
  public String getObject() {
    return object;
  }
  /* ----------------------------- */
  public void setSubject(String subject) {
	  this.subject = subject;
  }
  /* ----------------------------- */
  public void setPredicate(String predicate) {
	  this.predicate = predicate;
  }
  /* ----------------------------- */
  public void setObject(String object) {
	  this.object = object;
  }
  /* ----------------------------- */
  @Override
  public String toString() {
   return subject + " " + predicate + " " + object + " .";
  }
  /* ----------------------------- */
	@Override
					public boolean equals(Object obj) {
									if (obj == null) {
													return false;
									}
									if (obj == this) {
													return true;
									}
									if (!(obj instanceof Triple)) {
													return false;
									}
									Triple givenTriple = (Triple)obj;
									if (givenTriple.subject.equals(this.subject) &&
																	givenTriple.predicate.equals(this.predicate) &&
																	givenTriple.object.equals(this.object)) {
													return true;
									}
									return false;
					}
	/* ----------------------------- */
	@Override
					public int hashCode() {
									return new HashCodeBuilder(17, 37).
													append(this.subject).
													append(this.object).
													append(this.predicate).
													toHashCode();
					}
}
