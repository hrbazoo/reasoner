package owl;

public class Owl {
  public static final String SAMEAS = "<http://www.w3.org/2002/07/owl#sameAs>";
  public static final String EQUCLASS = "<http://www.w3.org/2002/07/owl#equivalentClass>";
  public static final String EQUPROPERTY = "<http://www.w3.org/2002/07/owl#equivalentProperty>";
}
