package rdfs;

public class Rdfs {
  public static final String CLASS = "<http://www.w3.org/2000/01/rdf-schema#Class>";
  public static final String SUBCLASSOF = "<http://www.w3.org/2000/01/rdf-schema#subClassOf>";
  public static final String SUBPROPERTYOF = "<http://www.w3.org/2000/01/rdf-schema#subPropertyOf>";
  public static final String DOMAIN = "<http://www.w3.org/2000/01/rdf-schema#domain>";
  public static final String RANGE = "<http://www.w3.org/2000/01/rdf-schema#range>";
  public static final String DATATYPE = "<http://www.w3.org/2000/01/rdf-schema#Datatype>";
}
